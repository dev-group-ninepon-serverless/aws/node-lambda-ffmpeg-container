require('dotenv').config()
const { exec, spawn, execSync } = require('node:child_process');
const fs = require('fs');
const ffmpeg = require("fluent-ffmpeg");
const path = require('path');

const AWS = require('aws-sdk')
const s3 = new AWS.S3({
    accessKeyId: process.env.AWS__ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS__SECRET_ACCESS_KEY,
    region: process.env.AWS__REGION,
})

exports.handler = async (event, context, callback) => {
    // debugging on local
    let body = event
    if(event.body){
        // on lambda
        body = JSON.parse(event.body)
    }

    const source_s3Key = body.source_s3Key
    const destination_s3Key = body.destination_s3Key

    const pathDestinationOnly = path.dirname(destination_s3Key);
    const fileSourceName = path.parse(source_s3Key).base;
    const fileDestinationName = path.parse(destination_s3Key).base;
    const pathTmpFile = `/tmp/${pathDestinationOnly}/`
    const lastTypeName = fileDestinationName.split(".")[1]

    const tmpWithSource = `${pathTmpFile}${fileSourceName}`
    const tmpWithDestination = `${pathTmpFile}${fileDestinationName}`

    try{
        await execSync(`rm ${tmpWithSource}`)
    }catch(err){ }
    try{
        await execSync(`rm ${tmpWithDestination}`)
    }catch(err){ }

    await execSync(`mkdir -p ${pathTmpFile}`)
    const getObjParams = {
        Bucket: process.env.S3__BUCKET,
        Key: source_s3Key,
    }
    
    const downloadToLocal = new Promise((resolve, reject) => {
        let readStream = s3.getObject(getObjParams).createReadStream();
        let writeStream = fs.createWriteStream(`${tmpWithSource}`);
        readStream.pipe(writeStream)

        writeStream.on('finish', () => {
            resolve();
        });
        writeStream.on('error', reject);
    });

    await downloadToLocal
        .then(async ()=>{
            console.log(`download to local was finish!`);
            const ConvertByFfmPeg = new Promise((resolve,reject)=>{
                if(lastTypeName !== "mp4"){
                    console.log(`start to convert file!`);
                    ffmpeg()
                    .input(tmpWithSource)
                    .inputOptions("-loop 1")
                    .videoCodec("libvpx-vp9")
                    .outputOptions("-t 5")
                    .output(tmpWithDestination)
                    .on("start", (commandLine) => {
                        console.log("Spawned Ffmpeg with command: " + commandLine);
                    })
                    .on("error", (err) => {
                        console.log("An error occurred: " + err.message);
                        reject()
                    })
                    .on("end", () => {
                        console.log("Processing finished !");
                        resolve()
                    })
                    .run();
                }else{
                    ffmpeg()
                    .input(tmpWithSource)
                    .videoCodec("libvpx-vp9")
                    .audioCodec("opus")
                    .output(tmpWithDestination)
                    .on("start", (commandLine) => {
                        console.log("Spawned Ffmpeg with command: " + commandLine);
                    })
                    .on("error", (err) => {
                        console.log("An error occurred: " + err.message);
                        reject()
                    })
                    .on("end", async () => {
                        console.log("Processing finished !");
                        resolve()
                    })
                    .run();
                }
            })

            await ConvertByFfmPeg
                .then(async ()=>{
                    const uploadS3 = new Promise( async (resolve, reject) => {
                        try{
                            const fileContent = fs.readFileSync(tmpWithDestination)
                            let writeParam = {...getObjParams, Body: fileContent, ACL: "public-read",}
                            writeParam.Key = destination_s3Key
                            const res = await s3.upload(writeParam).promise()
                            resolve(res)
                        }catch(err){
                            reject(err)
                        }
                    })

                    await uploadS3
                        .then(async (resUploadS3)=>{
                            console.log(`CONVERT FILE HAS SUCCEED!!`);
                            try{
                                await execSync(`rm ${tmpWithSource}`)
                            }catch(err){ }
                            try{
                                await execSync(`rm ${tmpWithDestination}`)
                            }catch(err){ }

                            console.log(`-----`);
                            console.log(resUploadS3);
                            console.log(`-----`);
                            return {statusCode: 200, body: JSON.parse( JSON.stringify(resUploadS3) ) }
                        })
                        .catch((err)=>{
                            return {statusCode: 500, body: JSON.stringify({ message: `error upload S3: ${err}` }) }
                        })
                })
                .catch(()=>{
                    return {statusCode: 500, body: JSON.stringify({ message: `error ffmpeg convert file` }) }
                })

        })
        .catch(()=>{
            return {statusCode: 500, body: JSON.stringify({ message: `error download to local` }) }
        })
};