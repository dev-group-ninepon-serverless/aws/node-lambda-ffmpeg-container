FROM public.ecr.aws/lambda/nodejs:16

# ENV AWS_DEFAULT_REGION us-east-1

RUN yum -y update
RUN yum -y install nano wget curl tar xz epel-release
# RUN yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
# RUN yum -y install amazon-linux-extras
# RUN yum -y install autoconf automake bzip2 bzip2-devel lcms2-devel libpng-devel cmake freetype-devel gcc gcc-c++ git libtool make pkgconfig zlib-devel

RUN mkdir -p /usr/local/bin/ffmpeg/
COPY ./ffmpeg/ /usr/local/bin/ffmpeg/
RUN ln -s /usr/local/bin/ffmpeg/ffmpeg /usr/bin/ffmpeg

# Copy function code
COPY app.js package.json /var/task/
# COPY app.js package.json ${LAMBDA_TASK_ROOT}

RUN npm install

# COPY ./entry_script.sh /entry_script.sh
# ADD aws-lambda-rie-x86_64 /usr/local/bin/aws-lambda-rie
# ENTRYPOINT [ "/entry_script.sh" ]

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD [ "app.handler" ]